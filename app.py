from flask import Flask, jsonify, request, render_template
import pymysql.cursors

#ALTER TABLE user ADD CONSTRAINT fk_id_equipamento FOREIGN KEY(id) REFERENCES equipamento (id_equipamento)

app = Flask(__name__)
connection = pymysql.connect(host='us-cdbr-east-04.cleardb.com',
                             user='b57c4db56c2ada',
                             password='d5259ff0',
                             db='heroku_37a5ebaf46a8230',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

##############################INDEX##########################################
@app.route('/')
def index():
        return render_template('index.html')
##############################SUCESS##########################################
@app.route('/submit', methods=['POST'])
def submit():
       # if request.method == 'POST':
       #         email    = request.form['email']
       #         password = request.form['password']
       #         print(email,password)
                return render_template('success.html')
##############################GET-ONE-USER###################################
@app.route('/userdata/<int:id>', methods=['GET'])
def datagetone(id):
        try:
                with connection.cursor() as cursor: 
                        cursor.execute("SELECT * FROM user WHERE id=%s", id)
                        row = cursor.fetchone()
                        resp = jsonify(row)
                        resp.status_code = 200
                        return resp
        except Exception as e:
                print(e)
        finally:
               cursor.close()             

##############################GET-ALL-USER###################################
@app.route('/userdata', methods=['GET'])
def datagetall():
        with connection.cursor() as cursor:
             result = "SELECT * FROM user "
             cursor.execute(result)
             cursor.close()
             resultado = cursor.fetchall()
             print(resultado)
             return jsonify(resultado), 200 
    

##############################POST-ONE-USER###################################
@app.route('/userdata', methods = ['POST'])
def  datapost():
    
     content = request.get_json()
     print (content['name'])
     print (content['email'])
     print (content['password'])     
     print (content['privilegio'])

     value1 = content['name']
     value2 = content['email']
     value3 = content['password']
     value4 = content['privilegio']    
   
     with connection.cursor() as cursor:
             sql = "INSERT INTO `user` (`email`,`password`, `name`, `privilegio`) VALUES (%s,%s,%s,%s)"
             cursor.execute(sql,(value1,value2,value3,value4)) 
             connection.commit()
             return 'JSON posted',200

##############################DELETE-ONE-USER###################################
@app.route('/userdata/<int:id>', methods=['DELETE'])
def delete_user(id):
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM user WHERE privilegio = 'operador' AND id=%s", (id,))
            if(cursor.execute != 0):
                    return jsonify({'message': 'Usuario Deletado com sucesso'}), 200
            else:
                    return jsonify({'message': 'Usuario não é administrador..!!'})

##############################UPDATE-ONE-USER###################################
@app.route('/userdataupdate', methods=['POST'])
def update_user():
           try:
                _json = request.json
                _id =_json['id']
                _email = _json['email']
                _password = _json['password']
                _name =_json['name']  
  
                if _id and _email and _password and _name and request.method == 'POST':
                                with connection.cursor() as cursor:
                                        sql = "UPDATE user SET email=%s, password=%s, name=%s WHERE id=%s"
                                        data = (_email, _password, _name, _id,)
                                        cursor.execute(sql,data) 
                                        connection.commit()
                                        resp = jsonify('Usuario Atualizado!')
                                        resp.status_code = 200
                                        return resp
                else:
                   return not_found()
           except Exception as e:
                 print(e)
           finally:
                cursor.close() 
  

###################################GET-ONE-EQUIP###################################################
@app.route('/equipdata/<int:id>', methods=['GET'])
def datagetequipone(id):
        try:
                with connection.cursor() as cursor: 
                        cursor.execute("SELECT * FROM equipamento WHERE id_equipamento=%s", id)
                        row = cursor.fetchone()
                        resp = jsonify(row)
                        resp.status_code = 200
                        return resp
        except Exception as e:
                print(e)
        finally:
               cursor.close()             

###################################GET-ALL-EQUIP###################################################
@app.route('/equipdata', methods=['GET'])
def datagetallequip():
        with connection.cursor() as cursor:
             result = "SELECT * FROM equipamento "
             cursor.execute(result)
             resultado = cursor.fetchall()
             print(resultado)
             return jsonify(resultado), 200 


###################################POST-ONE-EQUIP###################################################

@app.route('/equipdata', methods = ['POST'])
def  datapostequip():
    
     content = request.get_json()
     print (content['nome'])
     print (content['modelo'])
     print (content['status'])     
     
     value1 = content['nome']
     value2 = content['modelo']
     value3 = content['status']    
   
     with connection.cursor() as cursor:
             sql = "INSERT INTO `equipamento` (`nome`,`modelo`, `status`) VALUES (%s,%s,%s)"
             cursor.execute(sql,(value1,value2,value3)) 
             connection.commit()
             return 'JSON posted',200

##############################DELETE-ONE-EQUIP###################################
@app.route('/equipdata/<int:id>', methods=['DELETE'])
def delete_equip(id):
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM equipamento WHERE id_equipamento=%s", (id,))
            return jsonify({'message': 'Equipamento Deletado com sucesso'}), 200


##############################UPDATE-ONE-EQUIP###################################
@app.route('/equipdataupdate', methods=['POST'])
def update_equip():
           try:
                _json = request.json
                _id =_json['id']
                _nome = _json['nome']
                _modelo = _json['modelo']
                _status =_json['status']  
  
                if _id and _nome and _modelo and _status and request.method == 'POST':
                                with connection.cursor() as cursor:
                                        sql = "UPDATE equipamento SET nome=%s, modelo=%s, status=%s WHERE id_equipamento=%s"
                                        data = (_nome, _modelo, _status, _id,)
                                        cursor.execute(sql,data) 
                                        connection.commit()
                                        resp = jsonify('Equipamento  Atualizado!')
                                        resp.status_code = 200
                                        return resp
                else:
                   return not_found()
           except Exception as e:
                 print(e)
           finally:
                cursor.close() 

##############################UPDATE-ONE-USER###################################

if __name__ == '__main__':
     app.debug = True
     app.run(host='0.0.0.0')
